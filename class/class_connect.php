<?php

class ConnectDB
{
    private $host = "apriorit.loc";
    private $user = "oleg";
    private $password = "111";
    private $db = "apriorit";

    public $connection;

    function connect() {
        $this->connection = mysqli_connect($this->host, $this->user, $this->password, $this->db);
    }

    function closeConnect() {
        $this->connection = null;
    }
}