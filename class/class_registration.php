<?php

include_once "class_connect.php";

class AccountCreater
{
    private $login;
    private $email;
    private $password;
    private $maxLengthLogin;
    private $minLengthLogin;
    private $maxLengthPassword;
    private $minLengthPassword;

    public function __construct($login, $email,$password)
    {
        $this->login = htmlspecialchars($login);
        $this->email = htmlspecialchars($email);
        $this->password = htmlspecialchars($password);
        $this->maxLengthLogin = 10;
        $this->minLengthLogin = 4;
        $this->maxLengthPassword = 12;
        $this->minLengthPassword = 3;
    }

    public function register() {
        if($this->loginExists()) {
            return false;
        }
        else {
            if(!$this->loginValidation()) {
                if(!$this->passwordValidation()) {
                    if($this->emailValidation()) {
                        if($this->addAccount()) {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }

    private function addAccount() {
        $connect = new ConnectDB();
        $connect->connect();
        $query = "INSERT INTO users (login,email,password) VALUES ('$this->login','$this->email','$this->password')";
        $add = mysqli_query($connect->connection,$query);

        if($add) {
            return true;
        }
        else
            return false;

        $connect->closeConnect();
    }

    public function loginValidation() {
        if(strlen($this->login) >= $this->minLengthLogin && strlen($this->login) <= $this->maxLengthLogin) {
            $not_correct_symbols = "!@\"#№;$%^:&?*()-_+//|\\`.,";
            $reg = "/[a-Z0-9]/";
            $bool = false;
            for($i = 0;$i < strlen($this->login);$i++) {
                for($j = 0;$j < strlen($not_correct_symbols);$j++) {
                    if($this->login[$i] == $not_correct_symbols[$j]) {
                        $bool = true;
                    }
                }
            }
            if($bool) {
                if(preg_match($reg, $this->login)){
                    return false;
                }
                else
                    return true;
            }
            else {
                return false;
            }
        }else {
            return false;
        }
    }

    public function emailValidation() {
        if(filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else
            return false;
    }

    private function passwordValidation() {
        if(strlen($this->password) >= $this->minLengthPassword && strlen($this->password) <= $this->minLengthPassword) {
            $not_correct_symbols = "!@\"#№;$%^:&?*()-_+//|\\`.,";
            $reg = "/[a-Z0-9]/";
            $bool = false;
            for($i = 0;$i < strlen($this->login);$i++) {
                for($j = 0;$j < strlen($not_correct_symbols);$j++) {
                    if($this->password[$i] == $not_correct_symbols[$j]) {
                        $bool = true;
                    }
                }
            }
            if($bool) {
                if(preg_match($reg, $this->login)){
                    return false;
                }
                else
                    return true;
            }
            else {
                return false;
            }
        }else {
            return false;
        }
    }

    public function loginExists() {
        $connect = new ConnectDB();
        $connect->connect();
        $query = "SELECT * FROM users WHERE login ='$this->login'";
        $query_1 = mysqli_query($connect->connection,$query);
        $row[] = $query_1->fetch_assoc();
        if($row[0]['login'] == $this->login) {
            return true;

        }
        else {
            return false;
        }

        $connect->closeConnect();
    }

    public function passwordExists() {
        $connect = new ConnectDB();
        $connect->connect();
        $query = "SELECT * FROM users WHERE password ='$this->password'";
        $query_1 = mysqli_query($connect->connection,$query);
        $row[] = $query_1->fetch_assoc();
        if($row[0]['password'] == $this->password) {
            return true;

        }
        else {
            return false;
        }

        $connect->closeConnect();
    }
}

